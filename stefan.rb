class Coworker
  attr_accessor :name, :email, :age, :dog

  def initialize(attributes = {})  # Hash attributes {}
    @name  = attributes[:name]   #Instanzvariable @...
    @email = attributes[:email]
    @age = attributes[:age]
    @dog = attributes[:dog]
  end

  def formatted_email   # String anzeigen
    "#{@name} <#{@email}> Age: #{@age}  Dogname:: #{@dog}"
  end
end